'use strict';


const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');
var path = require("path");
console.log(NODE_ENV,path);
//let dd = require('def')
module.exports = {
    //context: __dirname + '/frontend',
    entry: {
        home: path.resolve(__dirname, "frontend",'home'),
        about: path.resolve(__dirname, "frontend",'about')
        //header: './header'
    },
    output: {
        path: __dirname + "/public",
        filename: '[name].js',
        library: "[name]"
    },
    watch: NODE_ENV == 'development',
    watchOptions: {
        aggregateTimeout: 100
    },
    //devtool: "cheap-inline-module-source-map"
    devtool: NODE_ENV == 'development' ? 'source-map' : null,
    plugins: [
        new webpack.NoErrorsPlugin(),
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        })
    ],
    resolve: {
        modulesDirectories: ['node_modules'],
        extensions: ['','.js']
    },
    resolveLoader: {
        modulesDirectories: ['node_modules'],
        modulesTemplates: ['*-loader','*'],
        extensions: ['','.js']
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel', // 'babel-loader' is also a legal name to reference
                query: {
                    cacheDirectory: true,
                    presets: ['es2015'],
                    plugins: ['transform-runtime']

                }

            }
        ]
    }
};

if(NODE_ENV == 'production'){
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        })
    );
}